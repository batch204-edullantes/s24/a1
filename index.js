let getCube = 4**3;

console.log(`The cube of 4 is ${getCube}`);


let address = ['Salawagan', 'Quezon', 'Bukidnon'];
let [barangay, municipality, province] = address;

console.log(`I live in ${barangay}, ${municipality}, ${province}.`);


let animal = {
	name: 'Lolong',
	type: 'crocodile',
	weight: '1075kgs',
	measurement: '20 ft 3 in'
}

let {name:animalName, type:animalType, weight:animalWeight, measurement:animalMeasurement} = animal

console.log(`${animalName} was a saltwater ${animalType}. He weighed at ${animalWeight} with a measurement of ${animalMeasurement}`);

let numbs = [1,2,3,4,5];

numbs.forEach((item,index) => {
	console.log(item);
})

function Dog(name, age, breed) {
  this.name = name;
  this.age = age;
  this.breed = breed;
}

let dog1 = new Dog('Xoxo', 2, 'Husky')
console.log(dog1);